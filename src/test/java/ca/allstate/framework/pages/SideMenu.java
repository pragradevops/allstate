package ca.allstate.framework.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SideMenu {
    private WebDriver driver;

    @FindBy(css = ".bsLeftNavigation nav>ul>li:nth-child(4)>a")
    private WebElement billing;

    @FindBy(css = ".bsLeftNavigation nav>ul>li:nth-child(4)>a+span>a")
    private WebElement expandBillingButton;

    @FindBy(css = ".bsLeftNavigation nav>ul>li:nth-child(4)>ul>li:nth-child(1)>a")
    private WebElement ebilll;

    @FindBy(css = ".bsLeftNavigation nav>ul>li:nth-child(4)>ul>li:nth-child(3)>a")
    private WebElement epay;
    @FindBy(css = ".bsLeftNavigation nav>ul>li:nth-child(4)>ul>li:nth-child(2)>a")
    private WebElement epolicy;

    @FindBy(css = ".bsLeftNavigation nav>ul>li:nth-child(4)>ul>li:nth-child(4)>a")
    private WebElement serviceNotification;

    public SideMenu(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public SideMenu expandBillingMenu(){
        this.expandBillingButton.click();
        WebDriverWait wait = new WebDriverWait(driver,10);
        wait.until(ExpectedConditions.visibilityOf(ebilll));
        return this;
    }



    public EBillPage clickEbill(){
        this.ebilll.click();
        return new EBillPage(driver);
    }

    public SideMenu clickOnBilling(){
        this.billing.click();
        return this;
    }


}
