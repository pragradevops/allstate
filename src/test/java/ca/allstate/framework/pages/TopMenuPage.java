package ca.allstate.framework.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TopMenuPage {
    private WebDriver driver;

    @FindBy(css = ".topnavInner>div>a")
    private WebElement exploreAllStateLink;

    @FindBy(xpath = "//div[@class='headerlinks']/ul/li[1]/a")
    private WebElement findAgentLink;

    @FindBy(xpath = "//div[@class='headerlinks']/ul/li[2]/a")
    private WebElement contactUs;

    public TopMenuPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver,this);
    }

    public ContactPage clickContactUs(){
        this.contactUs.click();
        return new ContactPage(driver);
    }

    public WebElement getExploreAllStateLink() {
        return exploreAllStateLink;
    }

    public WebElement getFindAgentLink() {
        return findAgentLink;
    }

    public WebElement getContactUs() {
        return contactUs;
    }
}
