package ca.allstate.framework.drivermanager;

import ca.allstate.framework.config.BrowserType;
import ca.allstate.framework.config.Config;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class DriverManager {
    private final Logger logger  = LogManager.getLogger(DriverManager.class);
    private WebDriver driver;
    private static DriverManager driverManager;
    private static String browser;
    private DriverManager(){
        init();
        if(Config.getProperty("browser").equals(BrowserType.CHROME)){
            logger.info(" Property browser is set to  {}  ", Config.getProperty("browser"));
            driver = new ChromeDriver();
        }else  if(Config.getProperty("browser").equals(BrowserType.FIREFOX)){
            driver = new FirefoxDriver();
        }else {
            driver = new ChromeDriver();
        }
    }
    private DriverManager(String broswer){
        init();

        if(broswer.equals(BrowserType.CHROME)){
            driver = new ChromeDriver();
        }else  if(broswer.equals(BrowserType.FIREFOX)){
            driver = new FirefoxDriver();
        }else {
            driver = new ChromeDriver();
        }
    }

    private void init(){
        System.setProperty("webdriver.chrome.driver", (String)Config.getProperty("chrome.executable"));
        System.setProperty("webdriver.gecko.driver", (String)Config.getProperty("gecko.executable"));
    }

    public static WebDriver getDriverInstance() {
       if(driverManager==null){
           driverManager = new DriverManager();
       }
       return driverManager.driver;
    }
    public static WebDriver getDriverInstance(String browser) {
        if(driverManager==null || browser!=null && !DriverManager.browser.equals(browser)){
            driverManager = null;
            driverManager = new DriverManager(browser);
            DriverManager.browser = browser;
        }
        return driverManager.driver;
    }
}
