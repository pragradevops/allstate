package ca.allstate.framework.listeners;

import ca.allstate.framework.reports.ExtentTestManager;
import com.aventstack.extentreports.Status;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class ReportListener implements ITestListener {
    @Override
    public void onTestStart(ITestResult result) {
        ExtentTestManager.startTest(result.getName());
        ExtentTestManager.getTest().log(Status.INFO, "Started Test " + result.getName());
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        ExtentTestManager.getTest().log(Status.PASS, "Test Passed "+result.getName());
        ExtentTestManager.endTest();
    }

    @Override
    public void onTestFailure(ITestResult result) {
        ExtentTestManager.getTest().log(Status.FAIL, "Test Failed "+result.getName());
        ExtentTestManager.endTest();
    }
}
