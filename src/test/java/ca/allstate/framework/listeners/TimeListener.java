package ca.allstate.framework.listeners;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.util.Date;

public class TimeListener implements ITestListener {
    private final static Logger logger = LogManager.getLogger(TimeListener.class);

    @Override
    public void onTestStart(ITestResult result) {
        logger.info("****STARTING TEST {} - Start Time {}" , result.getName(), new Date());
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        logger.info("****PASSED TEST {} - End Time {}" , result.getName(), new Date());
    }

    @Override
    public void onTestFailure(ITestResult result) {
        logger.info("****FAILED TEST {} - End Time {}" , result.getName(), System.currentTimeMillis());
    }
}
