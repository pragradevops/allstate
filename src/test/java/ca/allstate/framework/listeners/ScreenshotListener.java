package ca.allstate.framework.listeners;

import ca.allstate.framework.util.FrameworkUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class ScreenshotListener implements ITestListener {

    private final Logger logger  = LogManager.getLogger(ScreenshotListener.class);
    @Override
    public void onTestStart(ITestResult iTestResult) {

    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        FrameworkUtil.captureScreenShot(iTestResult.getName());
        logger.info("Screenshot capured for the Test {} at Location - {}", iTestResult.getName(), FrameworkUtil.getScreenShotFile(iTestResult.getName()));

    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        FrameworkUtil.captureScreenShot(iTestResult.getName());
        logger.info("Screenshot capured for the Test {} at Location - {}", iTestResult.getName(), FrameworkUtil.getScreenShotFile(iTestResult.getName()));

    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {

    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }

    @Override
    public void onStart(ITestContext iTestContext) {

    }

    @Override
    public void onFinish(ITestContext iTestContext) {

    }
}
