package ca.allstate.framework.config;

public class BrowserType {
    public static final  String CHROME="chrome";
    public static final  String FIREFOX="firefox";
    public static final  String IE="explorer";
    public static final  String EDGE="edge";
}
