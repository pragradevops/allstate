package ca.allstate.framework.config;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Properties;

public class Config {
    private static Properties properties;

    private Config() {
        Path path = Paths.get("src","test","resources","framework.properties");
        try {
            FileInputStream inputStream = new FileInputStream(path.toFile());
            properties = new Properties();
            properties.load(inputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Object getProperty(String key){
        if(properties==null){
            new Config();
        }
        return properties.get(key);
    }
}
