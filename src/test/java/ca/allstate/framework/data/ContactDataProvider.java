package ca.allstate.framework.data;

import org.testng.annotations.DataProvider;

import java.util.Iterator;

public class ContactDataProvider {

    @DataProvider(name = "contactData")
    public static Iterator<Object []> getContactData(){
        ExcelProvider provider = new ExcelProvider();
        return provider.getData("Contact").iterator();
    }
}
