package ca.allstate.framework.data;

import ca.allstate.framework.config.Config;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ExcelProvider {
    private Workbook workbook;
    String dataFile = (String)Config.getProperty("datafile");

    public ExcelProvider() {
        try {
            InputStream inputStream = new FileInputStream(dataFile);
            workbook = new XSSFWorkbook(inputStream);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Object[]> getData(String sheetName){
            List<Object []> data = new ArrayList<>();

        Sheet sheet = workbook.getSheet(sheetName);
        Iterator<Row> rowIterator = sheet.rowIterator();
        rowIterator.next();
        while (rowIterator.hasNext()){
            Row row = rowIterator.next();
            List<Object> cellData = new ArrayList<>();

            Iterator<Cell> cellIterator = row.cellIterator();
            while (cellIterator.hasNext()){
                Cell cell = cellIterator.next();

                if(cell.getCellTypeEnum()== CellType.STRING){
                    cellData.add(cell.getStringCellValue());
                }
                if(cell.getCellTypeEnum()== CellType.NUMERIC){
                    cellData.add(cell.getNumericCellValue());
                }
                if(cell.getCellTypeEnum()== CellType.BOOLEAN){
                    cellData.add(cell.getBooleanCellValue());
                }
                if(cell.getCellTypeEnum()== CellType.BLANK){
                    cellData.add(cell.getStringCellValue());
                }

            }
            data.add(cellData.toArray());

        }
        return data;
        
    }


}
