package ca.allstate.framework.testcases;

import ca.allstate.framework.config.Config;
import ca.allstate.framework.drivermanager.DriverManager;
import ca.allstate.framework.listeners.ReportListener;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners(ReportListener.class)
public class HomeTest {

    @Test
    public void tc1(){
        WebDriver driverInstance = DriverManager.getDriverInstance();
        driverInstance.get("https://facebook.com");

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driverInstance.quit();
    }


}
