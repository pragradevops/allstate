package ca.allstate.framework.testcases;

import ca.allstate.framework.data.ContactDataProvider;
import ca.allstate.framework.data.ExcelProvider;
import ca.allstate.framework.listeners.ScreenshotListener;
import ca.allstate.framework.util.FrameworkUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Listeners(ScreenshotListener.class)
public class DataTest extends BaseTest {
    String parent;


    @BeforeMethod
    public void setUp2() {
        driver.findElement(By.linkText("Send An Email")).click();
        parent =  driver.getWindowHandle();
        Set<String> windows = driver.getWindowHandles();

        for (String window : windows){
            if(!window.equals(parent)){
                driver.switchTo().window(window);
                break;
            }
        }
    }

    @Test(dataProvider = "contactData", dataProviderClass = ContactDataProvider.class)
    public void testName(Object [] data) throws Exception {

        for (int i = 0 ; i < data.length; i++){
            System.out.println(i + " ===> " + data[i]);
        }

        WebElement contactMethodSelect = driver.findElement(By.cssSelector(".contactPref > .styled-select >select"));
        Select methodSelect = new Select(contactMethodSelect);



        methodSelect.selectByValue((String)data[0]);

        Select titleSelect = new Select(driver.findElement(By.cssSelector(".titleDiv>select")));

        titleSelect.selectByValue((String)data[1]);

        WebElement firstName = driver.findElement(By.cssSelector(".titleDiv>select+input"));

        firstName.clear();
        firstName.sendKeys((String)data[2]);

        WebElement lastName = driver.findElement(By.name("ctl00$ContentPlaceHolder$txtLastName"));
        lastName.clear();

        lastName.sendKeys((String)data[3]);

        driver.findElement(By.name("ctl00$ContentPlaceHolder$txtEmail")).sendKeys((String)data[4]);
        driver.findElement(By.name("ctl00$ContentPlaceHolder$txtStreetAddress")).sendKeys((String)data[5]);
        driver.findElement(By.name("ctl00$ContentPlaceHolder$txtCity")).sendKeys((String)data[6]);
        new Select(driver.findElement(By.name("ctl00$ContentPlaceHolder$ddlState"))).selectByValue((String)data[7]);
        driver.findElement(By.name("ctl00$ContentPlaceHolder$txtZip")).sendKeys((String)data[8]);
        driver.findElement(By.name("ctl00$ContentPlaceHolder$txtCountry")).sendKeys((String)data[9]);
        FrameworkUtil.captureScreenShot("testName-intermediate");
        driver.findElement(By.name("ctl00$ContentPlaceHolder$txtPhone")).sendKeys((String)data[10]);
        new Select(driver.findElement(By.name("ctl00$ContentPlaceHolder$ddlBestTime"))).selectByValue((String)data[11]);
        driver.findElement(By.xpath("//input[@name='ctl00$ContentPlaceHolder$rdlAllstateCustomer' and @value='"+(String)data[12] +"']")).click();
        driver.findElement(By.cssSelector(".rdlRequestType tr  input[value='"+(String)data[13] +"']")).click();
        driver.findElement(By.name("ctl00$ContentPlaceHolder$txtComment")).sendKeys((String)data[14]);

        driver.findElement(By.name("ctl00$ContentPlaceHolder$btnSubmit")).click();

        String titleOnly = driver.findElement(By.className("TitleOnly")).getText();
        Assert.assertEquals("THANKS FOR YOUR MESSAGE!", titleOnly);
        System.out.println(Arrays.toString(data));
        Thread.sleep(5000);

    }

    @AfterMethod
    public void goBackContact() {
        driver.findElement(By.className("ctl00$ContentPlaceHolder$btnReturnToContactUs")).click();
    }

    @AfterSuite(alwaysRun = true)
    public void tearDown() {
            driver.quit();
    }
}
