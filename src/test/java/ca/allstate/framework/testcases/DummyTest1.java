package ca.allstate.framework.testcases;

import ca.allstate.framework.drivermanager.DriverManager;
import ca.allstate.framework.listeners.ReportListener;
import ca.allstate.framework.listeners.ScreenshotListener;
import ca.allstate.framework.listeners.TimeListener;
import ca.allstate.framework.reports.ExtentTestManager;
import ca.allstate.framework.util.FrameworkUtil;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.*;

import java.io.IOException;

@Listeners({ScreenshotListener.class, TimeListener.class, ReportListener.class})
public class DummyTest1 {

    private WebDriver driver;

    @BeforeSuite
    public void setUp() {
        driver = DriverManager.getDriverInstance();
    }

    @Test
    public void testName() {
        driver.get("https://allstate.com");
        Assert.assertTrue(driver.getTitle().toUpperCase().contains("ALLSTATE"));

    }

    @Test(priority = 2)
    public void testDummy() throws IOException {

    }

    @AfterSuite
    public void tearDown() {
        driver.quit();
    }
}


