package ca.allstate.framework.testcases;

import ca.allstate.framework.drivermanager.DriverManager;
import ca.allstate.framework.pages.ContactPage;
import ca.allstate.framework.pages.SideMenu;
import ca.allstate.framework.pages.TopMenuPage;
import com.google.common.annotations.GwtIncompatible;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class TopMenuTest extends BaseTest {

    TopMenuPage  topMenuPage;
    SideMenu sideMenu;

    @BeforeClass
    public void setUpLocal() {
        driver.manage().window().maximize();
        topMenuPage = new TopMenuPage(driver);
    }

    @Test
    @Ignore
    public void testFindAgentAttribute() {
        String attribute = topMenuPage.getFindAgentLink().getAttribute("aria-label");
        Assert.assertTrue(attribute.contains("Agent"));
    }

    @Test(enabled = false)
    public void testContact() throws Exception {
        ContactPage contactPage = topMenuPage.clickContactUs();

        WebElement productSelect = driver.findElement(By.id("navigationfooterProduct-quote"));
        productSelect.sendKeys("Re");

        driver.get("https://pragra.co/sel.html");

        WebElement cars = driver.findElement(By.name("cars"));
        Select carSelect = new Select(cars);
        if(carSelect.isMultiple()){
            carSelect.selectByValue("op");
            carSelect.selectByValue("ad");
        }

        Thread.sleep(3000);


        carSelect.deselectAll();
        carSelect.selectByIndex(1);
        carSelect.selectByIndex(3);

        WebElement alertButton = driver.findElement(By.xpath("//button[contains(text(),'Alerts')]"));

        System.out.println(alertButton.getCssValue("background-color"));
        System.out.println(alertButton.getCssValue("color"));
        System.out.println(alertButton.getCssValue("width"));
        System.out.println(alertButton.getCssValue("height"));

        Assert.assertEquals(alertButton.getCssValue("background-color"),"rgba(40, 167, 69, 1)");

        alertButton.click();

       //driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        WebDriverWait wait = new WebDriverWait(driver,20);

        Alert alert=wait.until(ExpectedConditions.alertIsPresent()) ;

        System.out.println(alert.getText());
        Thread.sleep(2000);
        alert.dismiss();
//        Select select = new Select(productSelect);
//        select.selectByVisibleText("Boat");

//        sideMenu = new SideMenu(driver);
//        Assert.assertTrue(driver.getTitle().contains("Contact"));
    }

    @Test(enabled = false)
    public void testActions() throws Exception {
        driver.get("https://www.cn.ca/en/");
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        WebElement yourIndustry = driver.findElement(By.id("ctl06__11d5f5cfe76d413_repMainNav_topLevelLink_1"));
        WebElement grains = driver.findElement(By.id("ctl06__11d5f5cfe76d413_repMainNav_topLevelLink_1"));
        WebElement grainPlan = driver.findElement(By.id("ctl06__11d5f5cfe76d413_repMainNav_repSecondLevel_1_repThirdLevel_2_thirdLevelLink_0"));

        Actions actions = new Actions(driver);
        actions.moveToElement(yourIndustry).moveToElement(grains).moveToElement(grainPlan).clickAndHold().build().perform();
        Thread.sleep(2000);

    }

    @Test(dependsOnMethods ="testContact" )
    @Ignore
    public void testEbill() {
        sideMenu.expandBillingMenu().clickEbill();
    }


    @Test
    public void doubleClick() throws Exception{
        //driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        WebDriverWait wait = new WebDriverWait(driver,30);
        WebElement dblclik = driver.findElement(By.id("dblclik"));
        Actions actions = new Actions(driver);
        actions.doubleClick(dblclik).build().perform();
        Alert alert = driver.switchTo().alert();
        System.out.println(alert.getText());
        alert.dismiss();

        if(wait.until(ExpectedConditions.textToBe(By.id("msg"),"Just Double Clicked"))){
            WebElement msg = driver.findElement(By.id("msg"));
            Assert.assertEquals("Just Double Clicked", msg.getText());

        }






    }

    @Test
    public void testDragNDrop() {
        WebElement src = driver.findElement(By.id("draggable"));
        WebElement target = driver.findElement(By.id("droppable"));
        WebElement msg = driver.findElement(By.cssSelector("#droppable>p"));
        Assert.assertEquals(msg.getText(),"Drop here");
        Actions actions = new Actions(driver);
        actions.dragAndDrop(src,target).build().perform();
        Assert.assertEquals(msg.getText(),"Dropped!");

    }

    @AfterSuite
    public void tearDown() throws Exception {
        Thread.sleep(8000);
        driver.quit();
    }
}
