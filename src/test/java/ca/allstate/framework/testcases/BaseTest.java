package ca.allstate.framework.testcases;

import ca.allstate.framework.drivermanager.DriverManager;
import ca.allstate.framework.pages.TopMenuPage;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

public class BaseTest {
    public WebDriver driver;

    @BeforeSuite
    public void setUp() {
        driver = DriverManager.getDriverInstance();
        driver.get("https://www.allstate.com/contactus.aspx");

    }
}
