package ca.allstate.framework.util;

import ca.allstate.framework.config.Config;
import ca.allstate.framework.drivermanager.DriverManager;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class FrameworkUtil {
    private static SimpleDateFormat dateformat = new SimpleDateFormat("YYYYmmDDHHMMSSS");
    private static Map<String, String> sreenShotlocation = new HashMap<>();

    private static void createScreenShotDir(){
        Path path = Paths.get((String)Config.getProperty("screenshot.dir"));
        if(!path.toFile().exists()){
            try {
                Files.createDirectories(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void createReportDir(){
        Path path = Paths.get((String)Config.getProperty("reports.dir"));
        if(!path.toFile().exists()){
            try {
                Files.createDirectories(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void captureScreenShot(String testMethodName){
        createScreenShotDir();
        WebDriver driver = DriverManager.getDriverInstance();
        String fileName = testMethodName+dateformat.format(new Date())+".png";
        Path path = Paths.get((String)Config.getProperty("screenshot.dir"), fileName);
        sreenShotlocation.put(testMethodName,path.toAbsolutePath().toString());
        //File diskFile = path.toFile();
        File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            Files.copy(screenshot.toPath(),path);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getScreenShotFile(String testName){
        return sreenShotlocation.get(testName);
    }

    public static String getReportFilename(){
        createReportDir();
        return Paths.get((String)Config.getProperty("reports.dir"))+"/TestReport_"+dateformat.format(new Date())+".html";
    }
}

