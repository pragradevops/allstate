import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;

public class ReadFile {
    public static void main(String[] args) {
        try {
            Path path = Paths.get("/Users/atinsingh/projects/git-session2/config-server","data.csv");
            List<String> lines = Files.readAllLines(path);

            Iterator<String> iterator = lines.iterator();
            iterator.next();
            while (iterator.hasNext()){
                String line = iterator.next();
                String[] split = line.split(",");
                for(int i = 0; i<split.length; i++){
                    System.out.println(split[i]);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
